---
layout: markdown_page
title: "Why Starter/Bronze?"
---
## GitLab Starter/Bronze

Starter / Bronze is designed for smaller individual teams where they don't need to support compliance and governance needs found in larger organizations.  GitLab Starter / Bronze can help your team accelerate delivery and improve efficiency with process automation, release prioritization and workflow optimization.

##  Left to right faster

Get work done, one task at a time. Enjoy the benefits of moving work from WIP to done & shipped faster. Supervise the complete lifecycle of your work and enhance its quality earlier. Target all root causes and enable your team to fuel their collaboration by measuring contribution efforts across the board and assessing gaps. Team up around issue boards and push forward together in one single application for all the DevOps requirements.

> **Improving code quality and project documentation performed by physicist and devs alike**.
>
> CERN, where the World Wide Web was devised and designed, powers the their
> more than 12k developers with Gitlab to perform 120k CI jobs a month in over > > 35k projects.
>
> “It's clearly a powerful tool to do our operations, code collaboration and
> record discussions on our development and deployment process. We can do more > > because we can handle more complex projects. As an individual, I'm able to
> be involved with several large projects because I can rely on GitLab, and
> the other development tools that we have deployed around GitLab, to keep
> track of things.”
>
> _**Alex Lossent**_
> Version Control Systems Service Manager, CERN IT department
> [Read CERN's full transformation story using Gitlab Starter](/customers/cern/)

Read all case studies [here](/customers/).

## A DevOps Platform at your service in one single app

One data model for all your assets. One single application to drive your team efforts in one direction: your clients. One single source of truth for all your teams. Wipe toolchain connection nightmare and simplify your DevOps platform. Make every single effort count.

{::comment}
TITLE
SUMMARY
NAME
TITLE
READ MORE
{:/comment}

# Starter Specific Features

#### Remove friction and move the needle together

| Distribute different tasks to different team members and let them work together to deliver results  | ![Multiple assignees](https://docs.gitlab.com/ee/user/project/issues/img/multiple_assignees_for_issues.png) |

| **Features** | **Value** |
|:---------|:------|
| [Weights](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html)         | Prioritize and move forward what moves the needle  |
| [Multiple assignees](https://docs.gitlab.com/ee/user/project/issues/multiple_assignees_for_issues.html)     | Get collaboration going by letting the different profiles in your teams work on the same place concurrently |
| [Related issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html)      | Browse fast connected issues across groups and projects. Birds-eye view of all progress in the same direction |

#### Context switching no more

| Toggle away distractions and let nothing get in your way | ![Focus mode](https://docs.gitlab.com/ee/user/project/img/issue_board_focus_mode.gif) |

| **Features** | **Value** |
|:---------|:------|
| [Configuration](https://docs.gitlab.com/ee/user/project/issue_board.html) | Keep your team's work together with different issue boards that live within the same project  |
| [Focus mode](https://docs.gitlab.com/ee/user/project/issue_board.html#focus-mode-starter) | Reduce potential context switching by getting rid of other elements in the UI |
| [Reorder issues](https://docs.gitlab.com/ee/user/project/issues/sorting_issue_lists.html) | Drag and drop you issue lists to drive your development efforts the way you want |

#### Granular permission settings to drive progress

| Securely control key actions allowing only selected profiles to perform them | ![Multiple approval rules](https://docs.gitlab.com/ee/user/project/merge_requests/img/approvals_premium_mr_widget.png) |

| **Features** | **Value** |
|:---------|:------|
| [Multiple approvers](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html#multiple-approval-rules-premium) | Multiple and selective sign off of merge request to speed up release in control   |
| [Merge approvals](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html) | A set of approvers can provide more trust to the eventual result of a merge being confirmed |
| [Code owners](https://docs.gitlab.com/ee/user/project/code_owners.html) | Select anyone from the team – not necessarily a developer – to have approval permissions and broaden the projects reach  |

#### Attack operational gaps in a targeted way

| Understand team contribution and identify opportunities | ![Group analytics](https://docs.gitlab.com/ee/user/group/contribution_analytics/img/group_stats_graph.png) |

| **Features** | **Value** |
|:---------|:------|
| [Contributor analytics](https://docs.gitlab.com/ee/user/group/contribution_analytics/) | Understand and assess the different operational gaps your projects may run into  |
| [Advanced search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html) | Nail down resource and component search to promote inner sourcing |
| [Push rules, restricted push & MRs](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html) | Automate and concatenate actions based on the commit messages of the pushes to repos  |

#### Code Quality & Visual Reviews

| Feedback in context and in the right Merge Request | ![In context feedback](https://docs.gitlab.com/ee/ci/review_apps/img/toolbar_feeback_form.png) |

| **Features** | **Value** |
|:---------|:------|
| [Code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) | Measure the impact of development practices and improve them faster  |
| [Visual reviews](https://docs.gitlab.com/ee/ci/review_apps/#visual-reviews-starter) | Let reviewers provide feedback in the live environment where changes have been applied: reliable and faster feedback loops |


<center><a href="/sales" class="btn cta-btn orange">Contact sales and learn more about GitLab Starter/Bronze</a></center>
