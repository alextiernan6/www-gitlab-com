---
layout: markdown_page
title: "Category Direction - Feature Flags"
---

- TOC
{:toc}

## Feature Flags

Feature Flags can be used as part of software development to enable a feature to be tested even before it is completed and ready for release. 
A feature flag is used to hide, enable or disable the feature during run time. 
The unfinished features are hidden (toggled) so they do not appear in the user interface. 
This allows many small incremental versions of software to be delivered without the cost of constant branching and merging. 

Feature flags unlock faster, more agile delivery workflows by providing
control over the flexibility of  deployment to a specific environment or audience. In addition it reduces risk to production, in case a problem is detected, 
because disabling the feature is as easy as turning off a switch. 

Feature Flags is built with an [Unleash](https://github.com/Unleash/unleash)-compatible
 API, ensuring interoperability with any other compatible tooling,
 and taking advantage of the various client libraries available for
 Unleash. Unleash have recently announced that they are spinning up a hosted (paid) option while maintaining their open source offering. We will be monitoring this closely. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AFeature%20Flags)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)
- [Vision Deep Dive](https://www.youtube.com/watch?v=U8ks-EggZMI&feature=youtu.be)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1295) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Now that we have released feature flags that support two different strategies,
 % rollout ([gitlab#8240](https://gitlab.com/gitlab-org/gitlab/issues/8240)) and userID ([gitlab#11459](https://gitlab.com/gitlab-org/gitlab/issues/11459)),
we are moving forward with using feature flags internally as part of our deployment process ([gitlab#26842](https://gitlab.com/gitlab-org/gitlab/issues/26842)). This will allow us to get internal feedback and improve feature flag functionality for both our internal customers and the wider GitLab community.

In addition, we are working on a powerful integration between feature flags and the issues and/or merge requests
that are affected by them. Since GitLab serves as a single application  tool, users who use our feature flags and issue management, can associate feature flags 
directly from the issue and vice versa and view the current deployment status
from any location ([gitlab#26456](https://gitlab.com/gitlab-org/gitlab/issues/26456)). This will provide visibility from the issue itself, and will let you know which feature flag is associated to it, it's status and percent rollout from the feature flag view, enabling you control and insights from wherever you wish to manage your feature flags.

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).

Our focus at the moment is on using the feature internally to deliver GitLab itself. This is driving new requirements to the base features that are out there, and also helping us to ensure the list for `complete` maturity is accurate. Our plan is for our feature flag solution to compete with other products on the market such as [LaunchDarkly](https://launchdarkly.com/) or [Rollout](https://rollout.io/). As we work towards `complete` maturity, our expectation is that our primary adopters of this feature will be pre-existing GitLab users looking for incremental value. For buyers who are considering replacing JIRA, and looking for something that integrates feature flags with issues, we can also provide a valuable solution as we head towards `complete` maturity.

Key deliverables to achieve this are:

- [% rollout for Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/8240) (Complete)
- [UserID-based access](https://gitlab.com/gitlab-org/gitlab/issues/11459) (Complete)
- [Add ability to associate feature flag with contextual issue/epic/MR](https://gitlab.com/gitlab-org/gitlab/issues/26456)
- [Feature Flag Gradual Rollout strategy based on groups](https://gitlab.com/gitlab-org/gitlab/issues/13308)
- [Multiple strategies per Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/35554)
- [Cookie-based access](https://gitlab.com/gitlab-org/gitlab/issues/11456)
- [A/B testing based on Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/34813)
- [Add Rule based Feature Flag rollout strategy support](https://gitlab.com/gitlab-org/gitlab/issues/33315)
- [Permissions for Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/8239)
- [Feature Flags user metrics](https://gitlab.com/gitlab-org/gitlab/issues/31442)

If you are interested in understanding the roadmap in a more granular fashion, you are welcome to follow these epics:
- [Refactor Feature Flags UX](https://gitlab.com/groups/gitlab-org/-/epics/2130)
- [Feature Flags Improvements - user action enhancements](https://gitlab.com/groups/gitlab-org/-/epics/2134)
- [Feature Flags Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2236)

## Competitive Landscape

Other feature flag products offer more comprehensive targeting and
configuration. The simplicity of our solution is actually a strength
compared to this in some cases, but there is some basic functionality
still to add. As we are rigorously working to close the gaps with the competitors, our next strategy to tackle will be the ability to configure feature flags based on groups [gitlab-ee#13308](https://gitlab.com/gitlab-org/gitlab-ee/issues/13308)

There is a detailed LaunchDarkly comparison from when the project
was first being conceived [here](https://docs.google.com/spreadsheets/d/1p3QhVvdL7-RCD2pd8mm5a5q38D958a-vwPPWnBT4pmE/edit#gid=0).

## Analyst Landscape

Analysts are recognizing that this sort of capability is becoming
more a part of what's fundamentally needed for a continuous delivery
platform, in order to minimize blast radius from changes. Often,
solutions in this space are complex and hard to get up and running
with, and they are not typically bundled or well integrated with CD
solutions. 

This backs up our desire to not overcomplicated the solution space
here, and highlights the need for guidance. [gitlab#9450](https://gitlab.com/gitlab-org/gitlab/issues/9450)
introduces new in-product documentation to help development and
operations teams learn how to successfully adopt feature flags.

## Top Customer Success/Sales Issue(s)

None yet, but feedback is welcome.

## Top Customer Issue(s)

- Our most popular customer issue is the ability to manage feature flags from an environment view [gitlab#9098](https://gitlab.com/gitlab-org/gitlab/issues/9098). Users would like to be able to get a high level view of which feature flags exist per environment. 

## Top Internal Customer Issue(s)

- Our top internal customer feature is [gitlab#26456](https://gitlab.com/gitlab-org/gitlab/issues/26456) which allows you to associate feature flags with issues. This gives the users the flexibility to see on the issue level which feature flags are associated, their status and their percent rollout all in the issue view, but also from the feature flag level lets you see which issues are linked to that feature flag. 
- Enabling Unleash in our codebase to enable internal use of our feature flag solution ([gitlab-ee#57943](https://gitlab.com/gitlab-org/gitlab-ce/issues/57943)) is our main focus, as part of our values we believe it is important for us to use our own features and collect feedback to improve the experience of the greater GitLab community. 

### Delivery Team

- Feature Flags: [framework#216](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/216) and [framework#32](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/32)

## Top Vision Item(s)

One of our main themes in CI/CD is [Progressive delivery](https://about.gitlab.com/direction/cicd/#progressive-delivery). Feature flags, by definition is a form of progressive delivery as it allows you to deploy code incrementally and control the audience that will receive the new code. Our top vision item is to allow simple monitoring and management of the feature flags in the system, which can become a complex take once there are many feature flags in place. A feature flag dashboard as described in [gitlab#2236](https://gitlab.com/groups/gitlab-org/-/epics/2236) will make this an easy task.
