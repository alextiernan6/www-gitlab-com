---
layout: markdown_page
title: "Product Direction - Enablement"
---

- TOC
{:toc}

This is the product vision for GDK, and it is currently a work in progress.

## Overview

The GDK category is responsible for the GitLab Development Kit, which is the primary method for users to set up a local development environment when contributing to GitLab.

## Themes

Work in progress.