---
layout: markdown_page
title: "Category Direction - Collection"
---

- TOC
{:toc}

## 🧺 Collection

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include use cases, personas,
and user journeys into this section. -->

* [Overall Vision](https://about.gitlab.com/direction/fulfillment)
* [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Atelemetry)
* [Maturity: Minimal](/direction/maturity/)
* [Documentation](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html)
* [All Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Atelemetry)

Please reach out to  Tim Hey, Interim Product Manager for Telemetry ([E-Mail](mailto:they@gitlab.com) if you'd like to provide feedback or ask
questions about what's coming.

## 🔭 Collection Vision

Data is becoming more and more important for building products. Unfortunately, it's tricky to implement usage tracking, and often requires either complex and confusing code/integrations, or expensive, proprietary tools (or both).

Everyone should be able to easily track how their users are using their product whether they are a small team working on an Android app or a vast, enterprise company.

When thinking about telemetry collection, we are guided by a few North Stars:

* ⭐️ **Transparency in Collection**: Compliance and privacy are critical considerations with any telemetry effort.  We should establish clear responsibilities for collecting data in a way that is transparent, compliant and respectful of our user's right to privacy. All of these processes and decisions will be [clearly and publically documented](https://gitlab.com/groups/gitlab-org/-/epics/1638).
* ⭐️ **Efficiency of Collection**: A large part of the challenge with our historical usage ping was the inability for contributors to GitLab to know how and when to use it. Our [collection mechanisms](https://gitlab.com/groups/gitlab-org/-/epics/1668) will be beautifully documented, easy to use and simple to understand.
* ⭐️ **Measurable Results from Collection**: The true value of data is shown in what you can learn from it. We intend to create [TaaS - Telemetry as a Service](https://gitlab.com/groups/gitlab-org/-/epics/1783) - to enable our users to get value out their own collected data, both from their own GitLab instances and also the applications that they build using GitLab.

<!-- ## 🎭 Target audience
An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.

### Parker (Product Manager) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)

* 🙂 **Minimal** - 
* 😊 **Viable** -  
* 😁 **Complete** - 
* 😍 **Lovable** - 

### Sasha (Software Developer) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)

* 🙂 **Minimal** -
* 😊 **Viable** -
* 😁 **Complete** -
* 😍 **Lovable** -

### Dana (Data Analyst) - [Persona Description](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#dana-data-analyst)

* 🙂 **Minimal** -
* 😊 **Viable** -
* 😁 **Complete** -
* 😍 **Lovable** -

For more information on how we use personas and roles at GitLab, please [click here](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/). 
-->

## 🚀 What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

#### 🔥 Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ | [Define and measure monthly active users, overall and per-stage](https://gitlab.com/groups/gitlab-org/-/epics/1374) | After this epic is closed, we should have an internally consistent view of MAU and SMAU across GitLab.com and self-managed. We should be able to measure active use in a Periscope dashboard, enabling us to improve MAU and SMAU. We can then tackle improving this further with [SMAU/MAU v2.0.](https://gitlab.com/groups/gitlab-org/-/epics/1440) |

#### 🎉 Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ | [SMAU/MAU v2.0.](https://gitlab.com/groups/gitlab-org/-/epics/1440) | As our organization grows, we require better data to inform our product, marketing, and sales team as they make decisions to grow the business and realize our strategic goals. This epic will serve as the aggregation of issues required to improve our monthly active user metrics, so we can have a world-class data platform at GitLab. |
| 3️⃣ | [Improve telemetry data collection from self-managed instances](https://gitlab.com/gitlab-org/telemetry/issues/34) | Currently we have little to no visibility into how many of our largest and most valuable customers are using GitLab. We need to understand how we can collect data more consistently from our Self-Managed users in order to better serve them.  |
| 4️⃣ |[Telemetry Documentation](https://gitlab.com/groups/gitlab-org/-/epics/1638)|  It is important that as we roll out new changes and develop processes and workflows, we clearly and transparently document everything in a way that is easily discoverable and digestible by both GitLab team members and users/customers.  |

<!--  ## 🏅 Competitive landscape
The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

<!-- ## Analyst landscape
What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ## Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!--  ## 🎢 Top user issues
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!-- ## 🦊 Top internal customer issues/epics
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

<!--  ## Top Vision Item(s)
What's the most important thing to move your vision forward?-->

#### A brief history of Usage Ping

Historically, GitLab has utilized a primitive and limited product usage tracking system (developed in-house) called usage ping. This consists primarily of _counts_, e.g. how many pipelines, issues, projects, etc. that a user has in their instance/group. A snapshot of this data is sent once a week to GitLab's [Version application](https://gitlab.com/gitlab-org/version-gitlab-com) (which is also managed by Fulfillment), this ping also includes a version check to understand what version of GitLab is currently implemented. Further [Documentation](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) on usage ping.

The above is where we are today, but we need to move towards becoming a more data-informed Product organization at GitLab which requires a much more robust system for product usage data collection.

##### Handy Usage Ping Links:

* [Feature Instrumentation](https://about.gitlab.com/handbook/product/feature-instrumentation/)
* [usage_data.rb](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/lib/gitlab/usage_data.rb)
