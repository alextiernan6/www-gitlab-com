---
layout: markdown_page
title: "Inclusion Training"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is fundamental for a successful inclusive environment

We are **all responsible** for creating and maintaining an inclusive environment at GitLab.  

We all need to feel **empowered** contribute and collaborate

## Diversity & Inclusion at GitLab 

Diversity are all the differences we each have whether it be where we grew up, where went to school, experiences, how we react, age, race, gender, national origin, things we can and can not see, the list goes on.

Inclusion is understanding or recognizing all these differences and inviting someone to be a part of things and or collaborate, etc.

## What inclusive behaviors look like

1. Include and seek input from team members across a wide variety of backgrounds.
1. Active listening - Listen carefully to the person speaking and playback what they have said in an effort to show understanding.  
1. Make a habit of asking questions
1. If you have a strong reaction to someone ask yourself why.  Look inward.
1. Address misunderstandings and quickly resolve disagreements.
1. Make a point to understand each team members contribution efforts and leverage them as much as possible.
1. Ensure all voices are heard.  Include everyone as much as possible in discussions.
1. Assume positive intent and examine your assumptions/judgements.

See our values page for futher information on [inclusive language](https://about.gitlab.com/handbook/values/#inclusive-language--pronouns). 

## Session

In December 2019, we held 3 sessions on Inclusion Training. Below you can find a recorded session that follow along with the [slide deck](https://docs.google.com/presentation/d/1WujXXxNDorIXB3NJeEnneC0dnOoqdFtcL6OhM-zWt4s/edit?usp=sharing) and [agenda](https://docs.google.com/document/d/1za96EEONFnOp-cI1kflIstE-MIAKSlsYZbivjnhr6ys/edit?usp=sharing). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gsQ2OsmgqVM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
